import React, { useState } from 'react';
import './App.css';

function App() {

  const [entry, setEntry] = useState('');
  const [stack, setStack] = useState([]);
  const [submitClicked, setSubmitClicked] = useState(false);

  const inputHandler = (event) => {
    event.preventDefault();
    setEntry(event.target.value);
  }

  const submitHandler = () => {
    const tempEntry = entry.split('');
    const entryArrLength = tempEntry.length;
    const stack = [];

    {entryArrLength % 2 !== 0 && alert('Brackets are not matching')}

    tempEntry.map((eachElement)=>{
        (eachElement === '{' || eachElement === '[' || eachElement === '(') && 
          stack.push(eachElement);
          
        if(eachElement === '}'){
          if(stack[stack.length-1] === '{'){
            stack.pop();
          }
          if(stack[stack.length-1] === '[' || stack[stack.length-1] === '('){
            return;
          }
        }

        if(eachElement === ']'){
          if(stack[stack.length-1] === '['){
            stack.pop();
          }
          if(stack[stack.length-1] === '{' || stack[stack.length-1] === '('){
            return;
          }
        }

        if(eachElement === ')'){
          if(stack[stack.length-1] === '('){
            stack.pop();
          }
          if(stack[stack.length-1] === '[' || stack[stack.length-1] === '{'){
            return;
          }
        }
    })
    setStack(stack);
    setSubmitClicked(true);
  }
  
  return (
    <div className="App">
      <h1>
        Match the brackets
      </h1>
      <input
        type='text'
        onChange={inputHandler}
        placeholder="enter the brackets"
      />
      <button onClick={submitHandler}>Submit</button>
      <div>
        {(submitClicked && stack.length===0) && 'Brackets do match'}
        {(submitClicked && stack.length!==0) && 'Brackets do not match'}
      </div>
    </div>
  );
}

export default App;
